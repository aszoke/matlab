# aszoke's Matlab toolboxes

## About

In the following links you can find my Matlab toolboxes. These toolboxes implement some algorithms that were important in my research. The available toolboxes are:

**Note**: *Due to the Bitbucket HTML preview restriction (see [HTML rendering for generated doc](https://bitbucket.org/site/master/issue/6353/html-rendering-for-generated-doc)), in order to view the Test run samples in HTML, use should use the [GitHub & BitBucket HTML Preview](http://htmlpreview.github.io/) service. (Copy the link below and paste it into the text box that can be found in the service page.)*

* [Clustering Toolbox](https://bitbucket.org/aszoke/clustering_toolbox) contains clustering algorithms. Clustering is the task of grouping a set of objects in such a way that objects in the same group (called a cluster) are more similar (in some sense or another) to each other than to those in other groups (clusters).

    **Test run sample**:[test_clustering_toolbox.html](https://bitbucket.org/aszoke/clustering_toolbox/raw/master/html/test_clustering_toolbox.html)

    **Test run sample**:[test_dynamicprog.html](https://bitbucket.org/aszoke/clustering_toolbox/raw/master/dp/html/test_dynamicprog.html)

* [Graph Toolbox](https://bitbucket.org/aszoke/graph_toolbox) contains useful algorithms including graph traversal (BFS, DFS), topological ordering and includes some metrics calculations.

    **Test run sample**:[test_graph_toolbox.html](https://bitbucket.org/aszoke/graph_toolbox/raw/master/html/test_graph_toolbox.html)

* [Packing Toolbox](https://bitbucket.org/aszoke/packing_toolbox) contains knapsacking and binpacking algorithms. Knapsacking and binpacking problems are a class of optimization problems that involve attempting to pack objects together into containers.

    **Test run sample**:[test_packing_toolbox.html](https://bitbucket.org/aszoke/packing_toolbox/raw/master/html/test_packing_toolbox.html)

* [Scheduling Toolbox](https://bitbucket.org/aszoke/scheduling_toolbox) contains basic algorithms to solve optimization problems in which jobs are assigned to resources at particular times.

    **Test run sample**:[test_scheduling_toolbox.html](https://bitbucket.org/aszoke/scheduling_toolbox/raw/master/html/test_scheduling_toolbox.html)

* [Sorting Toolbox](https://bitbucket.org/aszoke/sorting_toolbox) contains algorithms that are used to put elements of a list in a certain order.  Efficient sorting is important for optimizing the use of other algorithms which require input data to be in sorted lists.

    **Test run sample**:[test_sorting_toolbox.html](https://bitbucket.org/aszoke/sorting_toolbox/raw/master/html/test_sorting_toolbox.html)

* [Software Estimation Toolbox](https://bitbucket.org/aszoke/sw_estimation_toolbox) contains algorithms to predict the most realistic use of effort required to develop or maintain software. 

    **Test run sample**:[test_sw_estimation_toolbox.html](https://bitbucket.org/aszoke/sw_estimation_toolbox/raw/master/html/test_sw_estimation_toolbox.html)

**Author**: Akos Szoke <aszoke@mit.bme.hu>

**Web site**: [http://www.kese.hu](http://www.kese.hu)

**Toolbox Source**: [https://bitbucket.org/aszoke/](https://bitbucket.org/aszoke/matlab/)

## License

([The MIT License](http://opensource.org/licenses/MIT))

Copyright (c) 2011 Akos Szoke

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.